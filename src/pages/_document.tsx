import { Html, Head, Main, NextScript } from 'next/document'
import {photoAnim} from "@/main/animations";
import {motion} from "framer-motion";
import React from "react";
export default function Document() {
  return (
    <Html lang="en">
        <Head >
            <link href="https://fonts.cdnfonts.com/css/clash-display" rel="stylesheet"/>
        </Head>
      <body>
        <Main/>
        <NextScript />
      </body>
    </Html>
  )
}
