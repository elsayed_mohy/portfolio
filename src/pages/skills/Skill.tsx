import React from 'react';
import Image from "next/image";
import classes from "./skills.module.scss"
import {container, fade} from "@/main/animations";


interface SkillProps {
    color:string;
    icon:string;
    name:string;
}

function Skill(props : SkillProps) {
    const {motion} = require('framer-motion')
    return (
        <motion.div variants={container} initial='hidden' animate='show' className={classes.skillContainer}>
            <motion.div variants={fade} className={classes.skillIconContainer}
                 style={{borderColor:`rgba(${props.color},0.5)`,backgroundColor:`rgba(${props.color},0.06)`}}>
                <Image  alt={props.name} src={props.icon} width={40} height={40}/>
            </motion.div>
            <motion.p variants={fade} style={{color:`rgb(${props.color})`}}>{props.name}</motion.p>

        </motion.div>

    );
}

export default Skill;
