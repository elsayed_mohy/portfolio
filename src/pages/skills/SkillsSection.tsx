import React from 'react';
import Skill from "@/pages/skills/Skill";
import classes from "./skills.module.scss"
import {motion} from "framer-motion";
import {container, fade, titleAnim, titleSideAnim} from "@/main/animations";

interface SkillsSectionProps {
        title:string;
        skills:any;

}
function SkillsSection(props : SkillsSectionProps) {

    return (
        <motion.div  style={{display:"flex",flexDirection:"column",alignItems:"flex-start"}}>
            <motion.div  className={classes.skillsSectionTitle}>
            <motion.h3 variants={titleSideAnim}  className='stroked-text'>{props.title}</motion.h3>
            </motion.div>
            <div className={classes.skillsContainer}>
                    {props.skills && props.skills.map((skill,index)=>(
                        <Skill key={index}    color={skill.color} icon={skill.icon} name={skill.name}/>
                    ))}
            </div>
        </motion.div>
    );
}

export default SkillsSection;
