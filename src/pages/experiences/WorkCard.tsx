import React, {useEffect, useState} from 'react';
import classes from "@/pages/experiences/experiences.module.scss";
import {motion} from "framer-motion";
import {containerVariants, fade} from "@/main/animations";

interface UsedSkill{
 title:string;
}

interface WorkProps {
    client:string;
    position:string,
    period:string,
    workType:string,
    description:string,
    usedSkills: UsedSkill[];
}

function WorkCard(props : WorkProps) {
    return (
        <motion.div variants={containerVariants} initial='hidden' animate='show' className={classes.workContainer}>
            <motion.div className={classes.workContainerHeader}>
                <motion.div>
                    <motion.p variants={fade} className={classes.place}>{props.client}</motion.p>
                    <motion.p variants={fade} className={classes.position}>{props.position}</motion.p>
                </motion.div>
                <motion.div className={classes.period_container}>
                    <motion.p variants={fade} className={classes.period}>{props.period}</motion.p>
                    <motion.p variants={fade} className={classes.workType}>{props.workType}</motion.p>
                </motion.div>
            </motion.div>
            <motion.p variants={fade} className={classes.description}>{props.description}</motion.p>
            <motion.div className={classes.usedSkills}>
                {props.usedSkills && props.usedSkills.map((skill,index)=>(
                    <motion.button variants={fade} key={index}>{skill.title}</motion.button>
                )) }
            </motion.div>
        </motion.div>

    );
}

export default WorkCard;
