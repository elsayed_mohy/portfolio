import React from 'react';
import classes from "@/pages/experiences/experiences.module.scss";
import WorkCard from "@/pages/experiences/WorkCard";
import experiencesData  from "@/pages/experiences/experiences.json";
import {motion} from "framer-motion";
import { titleSideAnim} from "@/main/animations";

function Works(props) {
    return (
        <motion.div className={classes.experiencesContainer}>
            <motion.div className={classes.experiencesSectionTitle}>
                <motion.h2 variants={titleSideAnim} initial='hidden' animate='show' className='stroked-text'>Works</motion.h2>
            </motion.div>
            {experiencesData.experiences.map((experience,index)=>(
                <WorkCard key={index}  client={experience.client} description={experience.description}
                          period={experience.period} position={experience.position} usedSkills={experience.usedSkills}
                          workType={experience.workType}/>
            ))}
        </motion.div>
    );
}

export default Works;
