import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import Header from "@/main/sections/Header/Header";
import Nav from "@/main/sections/Nav/Nav";
import {AnimatePresence} from "framer-motion";
import React, {useEffect, useState} from "react";
import {Router} from "next/router";
import Loader from "../main/shared/components/loader/Loader"

export default function App({ Component, pageProps, router  }: AppProps) {

  return  <AnimatePresence mode='wait'
                          initial={false}
                          onExitComplete={() => window.scrollTo(0, 0)}
  >
    <>
    <Header/>
    <Component {...pageProps} key={router.asPath}/>
    <Nav/>
    </>
  </AnimatePresence>
}
