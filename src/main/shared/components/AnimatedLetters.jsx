import React from 'react';
import {motion} from "framer-motion";
import {banner, letterAni} from "@/main/animations";

export default function AnimatedLetters ({ title }) {
    return     <motion.h2
        className='row-title'
        variants={banner}
        initial='initial'
        animate='animate'>
        {[...title].map((letter,index) => (
            <motion.span
                key={index}
                className='row-letter'
                variants={letterAni}>
                {letter}

            </motion.span>
        ))}
    </motion.h2>
}
