import React from 'react';
import classes from "./Nav.module.scss"
import Link from "next/link";
import {useRouter} from "next/router";
import background from "../../../images/texture-dark.jpg"

function Nav(props) {
    const router = useRouter();
    const [visible , setVisible] = React.useState(false);
    const toggleNavigator = () => {
         document.querySelector<HTMLElement>(':root')!.style.setProperty('overflow', 'auto')
        const full_nav_content = document.getElementById('full_nav_content',) as HTMLButtonElement | null;
        if (full_nav_content != null) {
            full_nav_content.style.setProperty('display', visible ? 'none' :'block');
        }
        // document.getElementById<HTMLElement>("full_nav_content")!.style.setProperty('display', visible ? 'none' :'block')
        setVisible((prevState)=> !prevState)
    }
    React.useEffect(()=>{
        document.querySelector<HTMLElement>(':root')!.style.setProperty('overflow', 'auto')
        const full_nav_content = document.getElementById('full_nav_content',) as HTMLButtonElement | null;
        if (full_nav_content != null) {
            full_nav_content.style.setProperty('display', 'none')
        }
    },[])
    return (
<>
    <div className={classes.nav_container}><ul>
        <li className={router.pathname == "/" ? "active" : ""}><Link  href='/'>Home</Link></li>
        <li className={router.pathname == "/about" ? "active" : ""}><Link  href='/about'>About</Link></li>
        <li className={router.pathname == "/skills" ? "active" : ""}><Link  href='/skills'>Skills</Link></li>
        <li className={router.pathname == "/experiences" ? "active" : ""}><Link  href='/experiences'>Experiences</Link></li>
        <li className={router.pathname == "/contact" ? "active" : ""}><Link  href='/contact'>Contact</Link></li>

    </ul></div>
    <div className={classes.burger_icon} onClick={toggleNavigator}>
        <div></div>
        <div></div>
        <div></div>
    </div>
    <NavContent toggleNavigator={toggleNavigator}/>
</>
    );
}

interface NavContentInterface {
    toggleNavigator:any
}

function NavContent(props: NavContentInterface) {
    const router = useRouter();
    return (
        <div style={{backgroundImage:`url(${background.src})`}}  className={classes.full_nav_content} id='full_nav_content'>
                    <ul>
                        <li className={router.pathname == "/" ? "mob_active" : ""}><Link onClick={props.toggleNavigator} href='/'>Home</Link></li>
                        <li className={router.pathname == "/about" ? "mob_active" : ""}><Link onClick={props.toggleNavigator} href='/about'>About</Link></li>
                        <li className={router.pathname == "/skills" ? "mob_active" : ""}><Link onClick={props.toggleNavigator} href='/skills'>Skills</Link></li>
                        <li className={router.pathname == "/experiences" ? "mob_active" : ""}><Link onClick={props.toggleNavigator} href='/experiences'>Experiences</Link></li>
                        <li className={router.pathname == "/contact" ? "mob_active" : ""}><Link onClick={props.toggleNavigator} href='/contact'>Contact</Link></li>
                    </ul>
            <div className={classes.burger_icon} onClick={props.toggleNavigator}>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    );
}
export default Nav;
